'use strict';

var mongoose    = require('mongoose'),
    Category     = require('./../models/category');


exports.find = function(req, res){
    Category.find({}, function(err, category){
        if (err) return res.status(500).json(err);
        return res.status(200).json(category);
    });
}

exports.suggest = function(req, res){
    Category.find({name : new RegExp(req.params.search, 'i')}).exec(function(err, category){
        if (err) return res.status(500).json(err);
        return res.status(200).json(category);
    })
}
