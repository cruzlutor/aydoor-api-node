'use strict';

var mongoose    = require('mongoose'),
    redis       = require("redis"),
    client      = redis.createClient(),
    utils       = require('./../services/utils'),
    Category    = require('./../models/category'),
    Service     = require('./../models/service');


exports.findById = function(req, res){
    var id = req.params.id;
    Service.findById(id).populate('category').populate('user').exec(function(err, service){
        if (err) return res.status(500).json(err);
        if (!Service) return res.status(404).json(err);
        return res.status(200).json(service);       
    });
}

exports.findByUser = function(req, res){
    var id = req.params.id;
    Service.find({user:id}).populate('category').exec(function(err, service){
        if (err) return res.status(500).json(err);
        return res.status(200).json(service);  
    });
}

exports.search = function(req, res){
    var price       = [0, 1000];
    var category    = req.params.category;
    var country     = req.params.country;
    var city        = req.params.city;
    var hash        = country + '_' + city + '_' + category;
    
    // fix mix and max
    if (req.query.price){
        price = [parseInt(req.query.price[0]), parseInt(req.query.price[1])];
        console.log(price);
    }

    // console.log(req.params);
    client.get(hash, function(err, content){
        // if (content && !price ) return res.status(200).json(JSON.parse(content)); 

        Category.findById(category, function(err, category){
            if (err) return res.status(500).json(err);
            if (!category) return res.status(404).json({});
            Service.find({category:category.id, disabled:false, city:city, country:country, price: {$gte : price[0], $lte : price[1]}}).populate('category').populate('user', 'firstName avatar').exec(function(err, service){
                if (err) return res.status(500).json(err);
                // if (userService  && !price){
                //     client.set(hash, JSON.stringify(userService), redis.print);
                //     client.expire(hash, 3600);
                // }
                return res.status(200).json(service);  
            });   
        })
    });
}

exports.create = function(req, res){
    var image = (req.files.image) ? true : false;

    var Service = new Service({
        description:    req.body.description,
        type:           req.body.type,
        price:          req.body.price,
        user:           req.user.id,
        category:       req.body.category,
        images:         (image == true) ? [req.files.image.name] : [],
        city:           req.user.city,
        country:        req.user.country,
    });

    Service.save(function(err, service){
        if (err) return res.status(400).json(err);
        /* create new thumbnails */
        utils.createThumb(req.files.image, [450,450], function(){
            return res.status(200).json(service);
        });
    });
}

exports.update = function(req, res){
    var image = (req.files.image) ? true : false;

    Service.findById(req.params.id, function(err, service){
        if (err) return res.status(500).json(err);
        if (!service) return res.status(404).json({});
        if (service.user != req.user.id) return res.status(400).json(err);

        /* try to upload a thumbnail */
        utils.createThumb(req.files.image, [450,450], function(){
            if (err) return res.status(400).json(err);

            /* if image is true try to delete currents */
            if(image == true){
                try {
                    fs.unlinkSync('public/uploads/' + service.images[0]);
                    fs.unlinkSync('public/uploads/thumb-' + service.images[0]);
                }
                catch (e) {
                    console.log('no file found');
                }     
            }

            /* save new objects */
            service.description = req.body.description;
            service.price       = req.body.price;
            service.type        = req.body.type;
            service.disabled    = req.body.disabled;
            service.category    = req.body.category;
            service.images      = (image == true) ? [req.files.image.name] : service.images;
            
            service.save(function(err){
                if (err) return res.status(500).json(err);
                return res.status(200).json(service); 
            });

        });
    });
}

exports.delete = function(req, res){
    service.findById(req.params.id, function(err, service){
        if (err) return res.status(500).json(err);
        if (!service) return res.status(404).json(err);
        if (service.user != req.user.id) return res.status(400).json(err);

        service.remove();
        return res.status(200).json(service); 
    })
}