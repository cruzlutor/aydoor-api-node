'use strict';

var mongoose = require('mongoose'),
    Message  = require('./../models/message');
    

exports.chat = function(req, res){
    Message.find({ 
        $and : [
            { $or: [{to: req.user.id}, {to: req.query.to}] },
            { $or: [{from: req.query.to}, {from: req.user.id}] }
        ]}).populate('from to').exec(function(err, chat){
        if (err) return res.status(500).json(err);
        return res.status(200).json(chat);
    })
}

exports.create = function(req, res){
    var message = new Message({
        message:    req.body.message,
        from:       req.user.id,
        to:         req.body.to,
    });

    message.save(function(err, message){
        if (err) return res.status(500).json(err);
        return res.status(200).json(message);
    })
}