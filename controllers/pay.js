'use strict';

var mongoose    = require('mongoose'),
    Booking     = require('./../models/booking'),
    Service = require('./../models/service');


exports.book = function(req, res){
    var id = req.params.id;

    var cc = {
        number:     req.body.cc_number,
        cvc:        req.body.cc_cvc,
        month:      req.body.cc_month,
        year:       req.body.cc_year,
    }

    var book = {
        when:       req.body.when,
        amount:     req.body.amount,
        address:    req.body.address,
    }

    Service.findById(id).populate('service').exec(function(err, service){
        if (err) return res.status(500).json(err);
        if (!service) return res.status(404).json(err);


        var booking = new Booking({
            when:       req.body.when,
            provider:   service.user,
            client:     req.user.id,
            amount:     book.amount,
            place:{
                address: book.address,
            },
            price:      service.price,
            service:    service.service.id,
        });

        booking.save(function(err, booking){
            if (err) return res.status(500).json(err);
            return res.status(200).json(booking);
        });
 
    });
}
