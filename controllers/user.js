'use strict';
'use strict';

var mongoose    = require('mongoose'),
    fs          = require('fs'),
    easyimg     = require('easyimage'),
    User        = require('./../models/user'),
    utils       = require('./../services/utils'),
    Service     = require('./../models/service');


exports.findById =  function(req, res){
    User.findById(req.params.id, function(err, user){
        if (err) return res.status(500).json(err);
        if (!user) return res.status(404).json(err);
        user = user.serialize();
        return res.status(200).json(user); 
    })
}

exports.me = function(req, res){
    User.findById(req.user.id).exec(function(err, user){
        if (err) return res.status(500).json(err);
        if (!user) return res.status(404).json({});
        
        user = utils.serialize(user, [ '_id', 'email', 'country', 'city', 'firstName', 'lastName', 'avatar', 'phone', 'gender', 'about', 'is_provider']);
        return res.status(200).json(user); 
    });
}

exports.avatar = function(req, res){
    if (!req.files.avatar) return res.status(400).json({avatar:'debes agregar una imagen'});

    User.findById(req.user.id, function(err, user){
        if (err) return res.status(500).json(err);
        if (!user) return res.status(404).json({});

        utils.createThumb(req.files.avatar, [450,450], function(err){
            if (err) return res.status(500).json(err);

            /* delete old images */
            try {
                fs.unlinkSync('public/uploads/' + req.user.avatar);
                fs.unlinkSync('public/uploads/thumb-' + req.user.avatar)
            }
            catch (e) {
                console.log('no file found');
            } 

            user.avatar  = req.files.avatar.name;
            user.save(function(err){
                if (err) return res.status(500).json(err);
                user = utils.serialize(user, ['avatar']);
                return res.status(200).json(user); 

            })
        });
    });
}

exports.update = function(req, res){
    User.findById(req.user.id, function(err, user){
        if (err) return res.status(500).json(err);
        if (!user) return res.status(404).json({});
        
        user.firstName  = req.body.firstName;
        user.lastName   = req.body.lastName;
        user.phone      = req.body.phone;
        user.gender     = req.body.gender;
        user.about      = req.body.about;
        user.country    = req.body.country;
        user.city       = req.body.city;

        user.save(function(err){
            if (err) return res.status(500).json(err);
            Service.update({ user: user.id }, { $set: { country: user.country, city: user.city }}, { multi: true }, function(){});
            
            user = user.serialize();
            return res.status(200).json(user); 
        });
    });
}