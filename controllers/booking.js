'use strict';

var mongoose    = require('mongoose'),
    Booking     = require('./../models/booking');

/**
 * List of client bookings
 */
exports.find = function(req, res){
    if (!req.query.status) return res.status(500).json({});
    if (req.query.as != 'provider' && req.query.as != 'client' ) return res.status(500).json({});

    // check as
    var as = {};
    as[req.query.as] = req.user.id;

    // check criteria
    var status = [];
    if (req.query.status){
        req.query.status = req.query.status.split(",");
        for( var element in req.query.status){
            status[element] = {status: req.query.status[element]};
        }
    }

    // find booking
    Booking.find({
        $and: [
            as,
            { $or: status },
        ]
    }).populate('provider client service').exec(function(err, list){
        if (err) return res.status(500).json(err);
        return res.status(200).json(list);
    });
}

/**
 * Booking detail
 */
exports.findById = function(req, res){
    Booking.findById(req.params.id).populate('provider client service').exec(function(err, booking){
        if (err) return res.status(500).json(err);
        if (!booking) return res.status(404).json({});
        if (booking.provider.id != req.user.id && booking.client.id != req.user.id) return res.status(400).json({});
        return res.status(200).json(booking);
    })
}

/**
 * Create a new booking
 */
exports.create = function(req, res){
    var booking = new Booking({
        when:       req.body.when,
        provider:   req.user.id,
    });

    booking.save(function(err, booking){
        if (err) return res.status(500).json(err);
        return res.status(200).json(booking);
    });
}

/**
 * Accept an booking
 */
exports.accept = function(req, res){
    Booking.findById(req.params.id, function(err, booking){
        if (err) return res.status(500).json(err);
        if (!booking) return res.status(404).json({});
        if (booking.provider != req.user.id && booking.client != req.user.id) return res.status(400).json(err);

        booking.status = 'accepted';

        booking.save(function(err, booking){
            if (err) return res.status(500).json(err);
            return res.status(200).json(booking);
        });
        
    });
}

/**
 * Modify an booking date
 */
exports.modify = function(req, res){
    Booking.findById(req.params.id, function(err, booking){
        if (err) return res.status(500).json(err);
        if (!booking) return res.status(404).json({});
        if (booking.provider != req.user.id && booking.client != req.user.id) return res.status(400).json(err);
        
        booking.when = req.body.when;

        booking.save(function(err, booking){
            if (err) return res.status(500).json(err);
            return res.status(200).json(booking);
        });
        
    });
}

/**
 * Cancel an booking
 */
exports.cancel = function(req, res){
    Booking.findById(req.params.id, function(err, booking){
        if (err) return res.status(500).json(err);
        if (!booking) return res.status(404).json({});
        if (booking.provider != req.user.id && booking.client != req.user.id) return res.status(400).json(err);
        
        booking.status = 'canceled';

        booking.save(function(err, booking){
            if (err) return res.status(500).json(err);
            return res.status(200).json(booking);
        });
        
    });
}