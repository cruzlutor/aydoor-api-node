'use strict';

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    utils    = require('./../services/utils');

var categorySchema = new Schema({
    name:   { type: String, required:true },
    slug:   { type: String, required:true },
    image:  { type: String },
    
}, { versionKey: false} );

categorySchema.pre('save', function (next) {
    this.slug = utils.slugify(this.name);
    next(); 
});

module.exports = mongoose.model('Category', categorySchema);