'use strict';

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    utils    = require('./../services/utils');

var bookingSchema = new Schema({
    status:         { type: String, default: 'request' }, /* pending,accepted,finished,canceled */
    amount:         { type: Number, default: 1},

    update: { 
        when:       { type: Date, },
        user:       { type: Schema.Types.ObjectId, ref: 'User' },
        message:    { type: String },
        status:     { type: String, default: 'request' }, /* rejected */
    },

    place: { 
        address:    { type: String, required:true },
        lat:        { type: String },
        lon:        { type: String },
    },

    price:          { type: Number, get: utils.getPrice, set:utils.setPrice},

    /* population */
    client:         { type: Schema.Types.ObjectId, ref: 'User', required:true },
    provider:       { type: Schema.Types.ObjectId, ref: 'User', required:true },
    service:        { type: Schema.Types.ObjectId, ref: 'Service', required:true },
    
}, { versionKey: false} );

module.exports = mongoose.model('Booking', bookingSchema);