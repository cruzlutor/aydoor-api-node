'use strict';

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var messageSchema = new Schema({
    message:  { type: String, required:true },

    /* population */
    from:     { type: Schema.Types.ObjectId, ref: 'User', required:true },
    to:       { type: Schema.Types.ObjectId, ref: 'User', required:true },
    
}, { versionKey: false} );

module.exports = mongoose.model('Message', messageSchema);