'use strict';

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var invoiceSchema = new Schema({
    number:         { type: String },
    createdAt:      { type: Date, default: Date.now },

    /* user direction */
    firstName:      { type: String },
    lastName:       { type: String },
    postalCode:     { type: String },
    address:        { type: String },

    /* population */
    booking:        { type: Schema.Types.ObjectId, ref: 'Booking', required:true },
    
}, { versionKey: false} );

module.exports = mongoose.model('Invoice', invoiceSchema);