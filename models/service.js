'use strict';

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    utils    = require('./../services/utils');

var serviceSchema = new Schema({
    description:  { type: String, required: true },
    price:        { type: Number, get: utils.getPrice, set:utils.setPrice},
    status:       { type: String, default: 'published' }, /* published,unpublished,archived */
    disabled:     { type: Boolean, default: false },
    city:         { type: String, required: true },
    country:      { type: String, required: true },

    /* population */
    user:         { type: Schema.Types.ObjectId, ref: 'User', required: true },
    cateogry:     { type: Schema.Types.ObjectId, ref: 'Category', required: true },

}, { versionKey: false} );

module.exports = mongoose.model('Service', serviceSchema);