'use strict';

module.exports = {
    slugify: function(text){
        return text.toString().toLowerCase()
        .replace(/\s+/g, '-')        // Replace spaces with -
        .replace(/[^\w\-]+/g, '')   // Remove all non-word chars
        .replace(/\-\-+/g, '-')      // Replace multiple - with single -
        .replace(/^-+/, '')          // Trim - from start of text
        .replace(/-+$/, '');         // Trim - from end of text   
    },

    serialize: function(object, field){
        var _object = {};

        for(var i = 0; i < field.length; i++)
            _object[field[i]] = object[field[i]];

        return _object
    },

    createThumb: function(src, size, next){
        if ( !src ) return next(null);
        easyimg.thumbnail({
            src:'public/uploads/' + src.name, 
            dst:'public/uploads/thumb-' + src.name,
            width:size[0], height:size[1],
        }).then(function(){
            return next(null);
        }, function(err){
            return next(err);
        });
    },

    toLower: function(v) {
        return v.toLowerCase();
    },

    getPrice: function(num){
        return (num/100).toFixed(2);
    },

    setPrice: function(num){
        return num*100;
    },
} 