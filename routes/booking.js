'use strict';

var express     = require('express'),
    router      = express.Router(),
    policie		= require('./../services/policie');
    

/* import constrollers */
var bookingCtrl    = require('./../controllers/booking');


/* routes /api/booking */
router
	.get('/', policie, bookingCtrl.find)

	.post('/', policie, bookingCtrl.create)

	.get('/:id', policie, bookingCtrl.findById)

	.patch('/:id/accept', policie, bookingCtrl.accept)

	.patch('/:id/modify', policie, bookingCtrl.modify)

	.patch('/:id/cancel', policie, bookingCtrl.cancel);


module.exports = router;