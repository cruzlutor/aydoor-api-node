'use strict';

var express     = require('express'),
    router      = express.Router();


/* account */
router.use('/auth', require('./../routes/auth'));

/* user */
router.use('/user', require('./../routes/user'));

/* category */
router.use('/category', require('./../routes/category'));

/* services */
router.use('/service', require('./../routes/service'));

/* booking */
router.use('/booking', require('./../routes/booking'));

/* mesages */
router.use('/message', require('./../routes/message'));

/* payments */
router.use('/pay', require('./../routes/pay'));


module.exports = router;