'use strict';

var express     = require('express'),
    router      = express.Router(),
    policie		= require('./../services/policie');
    

/* import constrollers */
var messageCtrl    = require('./../controllers/message');


/* routes /api/message */
router
	.post('/', policie, messageCtrl.create)

	.get('/chat', policie, messageCtrl.chat);


module.exports = router;