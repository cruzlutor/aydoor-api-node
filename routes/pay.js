'use strict';

var express     = require('express'),
    router      = express.Router();
    

/* import constrollers */
var payCtrl    = require('./../controllers/pay');


/* routes /api/pay */
router
	.post('/book/:id', payCtrl.book);


module.exports = router;