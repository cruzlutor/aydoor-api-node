'use strict';

var express     = require('express'),
    router      = express.Router(),
    multer      = require('multer'),
    upload      = multer({ dest: 'uploads/' }),
    policie		= require('./../services/policie');
    

/* import constrollers */
var userCtrl    = require('./../controllers/user');


/* routes /api/user */
router
	.get('/me', policie, userCtrl.me)

	.put('/me', policie, userCtrl.update)

	.put('/avatar', policie, upload.single('avatar'), userCtrl.avatar)

	.get('/:id', userCtrl.findById);


module.exports = router;