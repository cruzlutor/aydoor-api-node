'use strict';

var express     = require('express'),
    router      = express.Router();
    

/* import constrollers */
var categoryCtrl    = require('./../controllers/category');


/* routes /api/category */
router
	.get('/', categoryCtrl.find)

	.get('/:search', categoryCtrl.suggest);


module.exports = router;