'use strict';

var express     = require('express'),
    router      = express.Router(),
    policie		= require('./../services/policie');
    

/* import constrollers */
var serviceCtrl    = require('./../controllers/service')


/* routes /api/service */
router
	.post('/', policie, serviceCtrl.create)

	.get('/:id', serviceCtrl.findById)

	.put('/:id', policie, serviceCtrl.update)

	.delete('/:id', policie, serviceCtrl.delete)

	.get('/user/:id', serviceCtrl.findByUser)

	.get('/search/:country?/:city?/:service?', serviceCtrl.search);



module.exports = router;